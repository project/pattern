<?php

namespace Drupal\pattern;


/**
 * Expose pattern as a function to twig.
 */
class PatternTwigFunction extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'pattern.patternFunction';
  }


  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('pattern', [$this, 'pattern']),
    ];
  }


  /**
   * Returns a pattern.
   *
   * @param string $category
   *   The category.
   * @param string $name
   *   The name of the pattern
   * @param array  $arguments
   *   The arguments.
   * @return array
   *   The render array.
   */
  public function pattern($category, $name, array $arguments = []) {
    return pattern($category, $name, $arguments);
  }

}
