# Readme

This small module will help integrating a pattern library (pattern lab, myagi, you name it) with a drupal backend. Instead of declaring a custom theme-hook and including a pattern into your custom template you can just call `pattern(<category>, <name>, <arguments>`.

## Installation

1. Install the module as usual

## Usage

To render a pattern, just use the `pattern`-helper-function:

in PHP:
```php
$render_array = pattern('atoms', 'button', ['label' => 'hello world']);
```

in twig:

```twig
{% set output = pattern('atoms', 'button', { label: 'hello world' } %}
{{ output }}
```


## Test module `test_pattern`

There is a second module part of the package, called test_pattern. It is just a sandbox to test the features of the pattern module under a given route. Do not enable it on production sites.

You can view the test output at `/test-pattern/example`
