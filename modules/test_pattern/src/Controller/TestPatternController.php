<?php

namespace Drupal\test_pattern\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for test_pattern routes.
 */
class TestPatternController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      'button' => pattern('test-patterns', 'button', [
        'label' => 'Hello world',
        'href' => 'https://www.cnn.com',
      ]),
      'template' => [
        '#theme' => 'test_pattern',
      ],
    ];

    return $build;
  }

}
